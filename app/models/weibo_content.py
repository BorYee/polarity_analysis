__author__ = 'BorYee'

from .. import db


class IndustryWeiboContent(db.Model):
    __tablename__ = 'tb_industry_weibo'

    id = db.Column(db.INT, primary_key=True, autoincrement=True)
    industry = db.Column(db.VARCHAR)
    sub_industry = db.Column(db.VARCHAR)
    key_word = db.Column(db.VARCHAR)
    nickname = db.Column(db.TEXT)
    content = db.Column(db.TEXT)
    user_link = db.Column(db.TEXT)
    content_link = db.Column(db.TEXT)

    def to_json(self):
        return {
            'id': self.id,
            'industry': self.industry,
            'sub_industry': self.sub_industry,
            'key_word': self.key_word,
            'nickname': self.nickname,
            'content': self.content,
            'user_link': self.user_link,
            'content_link': self.content_link
        }

    def __repr__(self):
        return '<weibo %s>' % self.content