__author__ = 'BorYee'

from flask import jsonify, request, current_app

from app.models.weibo_content import IndustryWeiboContent
from app.api_0_1 import api


@api.route('/industry/<string:industry>')
def query_weibo_by_industry(industry):
    weibo_json_list = []
    for weibo in IndustryWeiboContent.query.filter_by(industry=industry).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code':200,'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/sub_industry/<string:sub_industry>')
def query_by_sub_industry(sub_industry):
    weibo_json_list = []
    for weibo in IndustryWeiboContent.query.filter_by(sub_industry=sub_industry).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code':200,'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/key_word/<string:keyword>')
def query_by_key_word(keyword):
    weibo_json_list = []
    for weibo in IndustryWeiboContent.query.filter_by(key_word=keyword).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code':200,'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/search_weibo/<string:word>')
def search_weibo(word):
    weibo_json_list = []
    for weibo in IndustryWeiboContent.query.filter(IndustryWeiboContent.content.like('%' + word + '%')).all():
        weibo_json_list.append(weibo.to_json())
    res = {'code':200,'msg': weibo_json_list, 'size': weibo_json_list.__len__()}
    return jsonify(res)


@api.route('/')
def index():
    return 'Hello World!'
