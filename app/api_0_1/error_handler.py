__author__ = 'BorYee'

from flask import jsonify
from . import api


@api.app_errorhandler(404)
def page_not_found(e):
    return jsonify({'msg': [], 'size': 0, 'code': 404})