__author__ = 'BorYee'

from flask import request, jsonify
from . import api
from app.algorithms import social_media_polarity_analysis
import json, urllib


@api.route('/polarity/weibo/', methods=['POST', 'GET'])
def weibo_polarity():
    smpa = social_media_polarity_analysis.SentimentWordFrequencyBasedAnalysis('app/res/dict_jieba.txt')
    polarity = {}
    weibo = request.form
    for key in weibo:
        polarity[key] = smpa.predict_probability(weibo[key])
    res = {'code': 200, 'size': weibo.__len__(), 'polarity': polarity}
    return jsonify(res)
